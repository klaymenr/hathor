source $HATHOR/lib/libsmb

FIRMWARE_VERSION=`echo $PRODUCTVER | awk -F- '{print $3}'`
FIRMWARE_BRAND=`echo $PRODUCTVER | awk -F- '{print $2}'`

function hathor_get_firmware_name
{
	HATHOR_FIRMWARE_NAME="$PRODUCTNAME-$FIRMWARE_BRAND firmware $FIRMWARE_VERSION"
}

function hathor_get_firmware_path
{
	FIRMWARE_PATH_SERVER="\\\\athena\\SQA"
	FIRMWARE_PATH_SUFFIX="$FIRMWARE_BRAND\\Multi_lingual\\Application\\Firmware\\English"
	FIRMWARE_PATH_SERVER_REVERTED=`echo "$FIRMWARE_PATH_SERVER" | sed -e 's:\\\\:\/:g'`
	HATHOR_FIRMWARE_PATH_PREFIX="\\Project\\$PRODUCTNAME\\$FIRMWARE_PATH_SUFFIX"
	HATHOR_FIRMWARE_PATH="$FIRMWARE_PATH_SERVER\\$HATHOR_FIRMWARE_PATH_PREFIX\\$FIRMWARE_VERSION"
	HATHOR_FIRMWARE_PATH_PREFIX_REVERTED=`echo $HATHOR_FIRMWARE_PATH_PREFIX | sed -e 's:\\\\:\/:g'`
}

function hathor_get_specific_product_name
{
    local PRODUCTNAME=$1
    PRODUCT_WITH_SPECIFIC_NAMING="BD5171V FD8132V FD8151V FD8362E IP8133W IP8336W IP8136W IP8135W SD8332E SD8363E"
    for PRODUCT in $PRODUCT_WITH_SPECIFIC_NAMING; do

        if [ "$PRODUCTNAME" == "${PRODUCT:0:6}" ] ; then
            PRODUCTNAME=$PRODUCT
            break;
        fi

    done
    echo $PRODUCTNAME
}

function hathor_get_history_path 
{
	local SMBSERVER=$1
	local SMBPATH=$2
	local USER=$3
	local PASS=$4

	local get_history_succeed='true'
	local result=0

	export USER=$USER ; export PASSWD=$PASS

	local temp=`mktemp -d`
	pushd $temp 2>&1 > /dev/null
	SMB_CheckIfDirectoryExists $SMBSERVER $SMBPATH
	if [ "$ret" != "" ]; then
		get_history_succeed='false'

	elif [ "$ret" == "false" ]; then
		# if we are unable to find this product's directory. 
		# Fallback to special suffix version of firmware name

		ORIGINAL_PRODUCTNAME=$PRODUCTNAME
		PRODUCTNAME=`hathor_get_specific_product_name $PRODUCTNAME`
		hathor_get_firmware_path
		SMB_CheckIfDirectoryExists $FIRMWARE_PATH_SERVER_REVERTED $HATHOR_FIRMWARE_PATH_PREFIX_REVERTED
		if [ "$ret" == "false" ] || [ "$ret" != "true" ] ; then
			echo $ret
			return 1
		fi

		# bingo! we've successfully discovered the correct path by known path database
		# over-write SMBPATH 
		SMBPATH=$HATHOR_FIRMWARE_PATH_PREFIX_REVERTED
		# recover PRODUCTNAME
		PRODUCTNAME=$ORIGINAL_PRODUCTNAME
	fi

	if [ "$get_history_succeed" == "true" ]; then
		SMB_GetBunchOfFiles $SMBSERVER $SMBPATH "history.txt history.txt.lnk"

		if [ -f "history.txt.lnk" ] ; then
			HATHOR_FIRMWARE_HISTORY_PATH=`strings history.txt.lnk | grep "Project_ReleaseNote" | sed -e 's:\\\\:\/:g'`
			HATHOR_FIRMWARE_HISTORY_NAME=`basename $HATHOR_FIRMWARE_HISTORY_PATH`
			HATHOR_FIRMWARE_HISTORY_FOLDER=`dirname $HATHOR_FIRMWARE_HISTORY_PATH`
		else 
			HATHOR_FIRMWARE_HISTORY_PATH="$SMBPATH/history.txt"
			HATHOR_FIRMWARE_HISTORY_NAME="history.txt"
			HATHOR_FIRMWARE_HISTORY_FOLDER=`dirname $HATHOR_FIRMWARE_HISTORY_PATH`
		fi

	else
		result=1
	fi 

	unset USER; unset PASSWD

	rm -f history.txt history.txt.lnk
	popd 2>&1 > /dev/null && rm -rf $temp
	return $result
}

function hathor_get_firmware_file
{
	HATHOR_FIRMWARE_FILE="$PRODUCTVER.flash.pkg"
}

function hathor_get_firmware_end_date
{
	HATHOR_FIRMWARE_END_DATE=`date "+%Y/%m/%d"`
}

function hathor_get_author
{
	HATHOR_AUTHOR=`whoami`
}

function hathor_get_firmware_type
{
	echo $FIRMWARE_VERSION | grep "[0-9][0-9][0-9][a-z]$"
	[ "$?" = "0" ] && HATHOR_FIRMWARE_TYPE="General"
	[ "$?" = "1" ] && HATHOR_FIRMWARE_TYPE="Sample"
}

function hathor_get_firmware_version
{
	HATHOR_FIRMWARE_VERSION=$PRODUCTVER
}

function hathor_get_plugin_version
{
	source $HATHOR/lib/libpluginversion
}

function hathor_get_webpage_language
{
	HATHOR_WEBPAGE_LANGUAGE="de,en,es,fr,it,jp,pt,sc,tc"
}

function hathor_get_commonbug_list
{
	HATHOR_COMMONBUG_LIST=""
}

# tell vim to use shell script highlight automagically
# vim:ft=sh


